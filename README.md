# README    

This is a public project for custom Helm charts. 

At present, it contains the following custom Helm charts:

 - **airflow-jupyterhub**: a Helm chart combining the Bitnami Airflow and Bitnami JupyterHub charts  

## Usage

```bash
helm repo add enacos https://gitlab.com/api/v4/projects/35386814/packages/helm/stable/
helm install my-release enacos/airflow-jupyterhub
```
or use this chart as a subchart in a `Chart.yaml`:

```yaml
dependencies:
  - name: airflow-jupyterhub
    version: "0.1.0"
    repository: "https://gitlab.com/api/v4/projects/35386814/packages/helm/stable/"
```
