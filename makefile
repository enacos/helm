CHART_NAME=airflow-jupyter

clean:
	rm -rf charts/*

build: clean
	helm package $(CHART_NAME) --dependency-update

test:
	 helm package test --dependency-update

